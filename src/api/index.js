/** @format */

import home from './home';
import my from './my';
import common from './common';
export default {
  home,
  my,
  common,
};
