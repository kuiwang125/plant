/** @format */

export default [
  {
    name: 'serviceCount',
    method: 'GET',
    desc: '服务统计',
    path: '/apis/method/frappe.apis.apps.services.count',
  },
];
