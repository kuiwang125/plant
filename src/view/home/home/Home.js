import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { useTheme, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { size } from '@/utils';
import { Touchable, Icon, Button } from 'uiCommon';
export default () => {
  const { colors } = useTheme();
  const navigation = useNavigation();
  const barHeight = useSelector((state) => state.common.barHeight);
  return (
    <View style={[style.wrap, { paddingTop: barHeight }]}>
      <View style={style.topWrap}>
        <View style={style.topCon}>
          <Text style={style.title}>童话逼</Text>
          <Text style={style.status}>veg张书中</Text>
          <Text style={style.label}>该周期，植物需连续生长0/12小时</Text>
          <View style={style.userWrap}>
            <Text style={style.userText}>用户ID：600066</Text>
          </View>
          <View>
            <Image />
          </View>
        </View>
        <Button style={style.myWrap} />
        <View style={style.wrap}>
          <Button style={style.shareWrap} />
          <View>
            <Button style={style.houjian} />
            <Text>0.12</Text>
            <Text>e/h</Text>
          </View>
          <View>
            <Button style={style.th} />
            <Text>0.3</Text>
          </View>
        </View>
      </View>
    </View>
  );
};
const style = StyleSheet.create({
  wrap: {},
  topWrap: {},
  topCon: {},
  title: {},
  status: {},
  label: {},
  userWrap: {},
  userText: {},
  myWrap: {},
});
