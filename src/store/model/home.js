export function offerModel(data) {
  let {offer_list, top_offers} = data;
  return [...top_offers, ...offer_list];
}
