export function paylinkModel(data) {
  return data.map((item, index) => {
    return {
      ...item,
      key: new Date() + index,
      data: item.items,
    };
  });
}
