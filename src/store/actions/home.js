import { doAction } from './getData';

export const HOME_COUNT = 'HOME_COUNT';

export const getHomeCount = (params) => doAction(params, 'home/serviceCount', 'HOME_COUNT', 'homeCount');

export const commitWeekReportDetail = (params) => {
  return (dispatch) => {
    return dispatch({
      type: 'WEEK_REPORT_DETAIL',
      payload: { weekReportDetail: params },
    });
  };
};
