import { doAction } from './getData';

export const USER_INFO = 'USER_INFO';

export const commitUserInfo = (params) => {
  return (dispatch) => {
    return dispatch({
      type: USER_INFO,
      payload: { userInfo: params },
    });
  };
};

export const getUserInfo = (params) => doAction(params, 'my/userInfo', 'USER_INFO', 'userInfo');
