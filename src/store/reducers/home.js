import * as home from '../actions/home';

const INITIAL_STATE = {
  homeCount: {},
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case home.HOME_COUNT:
      return {...state, ...action.payload};

    default:
      return state;
  }
}
