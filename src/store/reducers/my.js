import * as my from '../actions/my';

const INITIAL_STATE = {
  userInfo: {},
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case my.USER_INFO:
      return {...state, ...action.payload};

    default:
      return state;
  }
}
