import Home from '@/view/home/home/Home';

const HomeRoute = {
  home: {
    screen: Home,
    name: 'home',

    options: { headerShown: false, title: '首页' },
  },
};
export default HomeRoute;
