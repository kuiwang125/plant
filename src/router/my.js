import My from '@/view/my';

import Login from '@/view/my/login/Login';

export default {
  my: {
    screen: My,
    name: 'my',
    options: { headerShown: true, title: '我的' },
  },

  login: {
    screen: Login,
    name: 'login',
    options: { headerShown: false, title: '登录' },
  },
};
